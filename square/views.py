from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from square.parsers import parse_int
from square.utils import generate_password, execute_query


def index(request):
    return HttpResponse("Hello, world!")


def get_password(request):
    try:
        length = parse_int(request, "length", 10, min=2, max=100)
    except ValueError as ex:
        return HttpResponse(str(ex), status=400)
    result = generate_password(length)
    return HttpResponse(result)


def get_sales(request):
    sql_query = 'SELECT UnitPrice, Quantity from invoice_items'
    result_sql = execute_query(sql_query)
    result = 0
    for track in result_sql:
        result += track[0] * track[1]
    return HttpResponse(result)


def get_genres(request):
    sql_query_genres = 'SELECT * from genres'
    sql_query = 'select genreid, milliseconds from tracks'
    result_sql_genres = execute_query(sql_query_genres)
    result_sql = execute_query(sql_query)
    genre_dict = {}
    genre_list = []
    genres = []
    l = {}
    for genre in result_sql_genres:
        genre_dict[genre[0]] = genre[1]
    for i in result_sql:
        i = list(i)
        i[0] = genre_dict[i[0]]
        genre_list.append(i)
    for i in genre_list:
        if i not in genres:
            genres.append(i[0])
            c = 0
            for j in genre_list:
                if j[0] == i[0]:
                    c += i[1]
            l[i[0]] = c
        else:
            pass
    l = str(l)

    return HttpResponse(l)

#comment to have difference to commit
