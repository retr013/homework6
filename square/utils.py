import random
import string
import os
import sqlite3


def generate_password(length):
    result = ''
    for i in range(length):
        result += random.choice(string.ascii_lowercase)
    return result


def execute_query(query, args=()):
    db_path = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    cur.execute(query, args)
    conn.commit()
    records = cur.fetchall()
    return records


def format_list(lst):
    response = ''
    for entry in lst:
        response += str(entry) + '<br>'
    return response



